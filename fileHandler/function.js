const mongoose = require('mongoose')
let config = require('../config/config.js')
let nodemailer = require('nodemailer')
let crypto = require('crypto')
var bcrypt = require('bcryptjs');
let saltRounds = 10;
let myPlaintextPassword = 's0/\/\P4$$w0rD'
let jwt = require('jsonwebtoken');
var Notification=require('../models/notificationSchema')
const cloudinary = require('cloudinary')
var apn = require("apn"),
options, connection, notification;
// create a passphrase random
var passphrase = require('passphrase');
//  twilio credentials ............................dubeyanuj639@gmail.com 
const accountSid = 'AC2f70a2338e2e98c22238204f8e8e2f46';
const authToken = '1dcfc1b53628ef167d1f92098a317998';
const client = require('twilio')(accountSid, authToken);
//  nexmo credentials ............................dubeyanuj639@gmail.com 
const Nexmo = require('nexmo')
const nexmo = new Nexmo({
    apiKey: '1ae2c8a3',
    apiSecret: '9hvObNUEuZkpQDeH'
});







// to hit any url (third party).............................................
var Client = require('node-rest-client').Client;
var nodeClient = new Client();



exports.responseHandler = (res, responseCode, responseMessage, data) => {
    res.send({
        responseCode: responseCode,
        responseMessage: responseMessage,
        data: data
    })
},

    exports.imageUploadToCloudinary = (imageB64, callback) => {
        console.log(imageB64)
        cloudinary.v2.uploader.upload(imageB64, (err, result) => {
            console.log("===>>>>in cloudinary function =====>>>", err, result);
            callback(result.url);
        })
    },



    exports.crypt = function (divPass) {
        console.log("calling function divPass====", divPass)
        const secret = 'Mobiloitte1';
        const hash = crypto.createHmac('sha256', secret)
            .update(divPass)
            .digest('hex');
        return hash;

    };

exports.bcrypt = function (divPass, cb) {
    // console.log("calling bcrypt funciton ====")
    bcrypt.genSalt(saltRounds, function (err, salt) {
        // console.log("calling bcrypt funciton 2 ====")
        bcrypt.hash(divPass, salt, function (err, hashPassword) {
            //   console.log("errrr   hash  ==>>",err,hashPassword)
            cb(null, hashPassword)
            // return hashPassword;
        });
    });


};

exports.bcryptVerify = (password, dbPassword, cb) => {
    console.log("=======in bcypt verify", password, dbPassword)
    bcrypt.compare(password, dbPassword, (err, res) => {
        if (err) {
            return commonFile.responseHandler(res, 400, "Invalid Credentials.")
        } else {
            console.log("null , response======== verify  password by bcrypt function =====>>>>", null, res)
            cb(null, res)
        }
    });
}


exports.jwt = (body, cb) => {
    console.log("calling jwt function ====", body)
    let token = jwt.sign(body, config.jwtSecretKey)
    console.log("token====", token)
    cb(null, token)

};

exports.jwtVerify = (req, res, next) => {
    // console.log("req.headers========", req.headers)
    if (req.headers.jwt == "null" || req.headers.jwt == "" || req.headers.jwt == "undefined" || req.headers.jwt == null || req.headers.jwt == undefined) {
        console.log("token missing")
        return commonFile.responseHandler(res, 400, "Token Missing")
    }

    jwt.verify(req.headers.jwt, config.jwtSecretKey, function (err, decoded) {
        if (err) {
            console.log("Invalid token")
            return commonFile.responseHandler(res, 400, "Token Invalid", err)
        } else {
            console.log("decode=====>>>>", decoded)
            next();
        }
    });


}


/*  Allow less secure apps: ON  from your gmail for sending gmail*/

exports.sendEmail = (email, subject, message, link, cc, bcc, callback) => {
    console.log("in send email for forgot password ==> ", email, subject, message, link)
    transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'ph-anuj@mobiloitte.com',
            pass: 'anuj11083'
        }
    })
    // console.log(message,"\n",link)
    let messageObj = {
        from: 'Noreply<ph-anuj@mobiloitte.com>',
        to: email,
        subject: subject,
        text: message, //"A sentence just to check the nodemailer",
        html: link, //"Click on this link to <a href=" + link + ">reset Password</a>",
        cc: cc,
        bcc: bcc
    }
    transporter.sendMail(messageObj, (err, info) => {
        console.log("in send mail second console-----", err, info)
        if (err) {
            console.log("Error occured", err)
            callback(null);
        } else {
            console.log("Mail sent")
            callback(null, info)
        }
    })
}

exports.balanceCal = async (contract, ownerAddress) => {
    // console.log("in balance cal function========",contract,ownerAddress)
    try {
        // console.log("====in try block====")
        var balance = await contract.methods.balanceOf(ownerAddress).call();
        // console.log("in try block ====>>", balance);
        return balance;
    } catch (err) {
        // console.log("====in catch block====", err)
        // throw err;
        return err;
    }
}

//  twilio for sending otp on mobiles.......................................................................... 
exports.sendMessage = (number, text, callback) => {
    client.messages.create({
        body: 'Your otp-  ' + "123642dfherw5",
        to: '+917451919823', // Text this number
        from: '+15868008694' // From a valid Twilio number
    }).then(data => {
        console.log("your otp is====>>", data)
        callback(null, data)
    }).catch(err => {
        console.log("in catch send otp to mobile by twilio===")
        callback(err)
    })
}

//  nexmo for sending otp on mobiles.......................................................................... 
exports.sendMessageNexmo = (number, otp, callback) => {
    callback(null, otp)
    return;
    // let otp = Math.floor(100000 + Math.random() * 900000) //six digit random number
    let from = '+918273242159';
    let to = '+91' + number;
    let text = 'Your OTP verification number is ' + otp;
    nexmo.message.sendSms(from, to, text, (error, response) => {
        if (error) {
            console.log("second")
            throw error;
        } else {
            console.log("response=====>>", response)
            callback(null, otp)
        }
    })
}
// create the passphrase........................................................................................
exports.createPass = (cb) => {
    var entropy = 175;//create 12 words passphrase
    passphrase(entropy, (_, phrase, actualEntropy) => {
        console.log('My passphrase is:', phrase);
        cb(null, phrase);
    });
}

// get the response from third party or etherscan................................................................
exports.nodeClient = (url, cb) => {
    /* to get all transaction from a account 
     var url = "http://api-ropsten.etherscan.io/api?module=account&action=txlist&address=" + address + "&sort=dsc"
     */
    // console.log("url ton hit===>>", url)
    nodeClient.get(url, (data, response) => {
        console.log("data==>>", data);
        cb(null, data)
        //   console.log("response===>",response);
    });
}

exports.sendNotification = (deviceToken, title, message, data, notiObj) => {



    var serverKey = 'AAAAdtyNEC0:APA91bFeZPCM-fslejcqzZHNrXE_fExyhkjqn5FzuXj4mJ3X9pkClFG9Hs0I76-pnIRmw512uEVBkhrMBzYF7FbqEirrVS6anw0uEuu8o3gzZG48hhCKlQrIEIZs36os5qTZiRU9b02r';
    var fcm = new FCM(serverKey);
    data["title"] = title;
    data["body"] = message;
    var payload = {

        to: deviceToken,
        "content_available": true,
        collapse_key: 'your_collapse_key',
        notification: {
            title: title,
            body: message,
            click_action: "fcm.ACTION.NOTIF"
        },
        data: data
    };

    fcm.send(payload, function (err, response) {
        if (err) {
        } else {
            let obj = {
                customerId: { cid: notiObj.userId, image: notiObj.image, name: notiObj.name },
                bussinessId: { bid: notiObj.businessManId },
                noti_type: 'CUSTOMER',
                eventId: notiObj.eventId,
                content: message,
                type: notiObj.type,
            };
            if (notiObj.eventStatus)
                obj.eventStatus = notiObj.eventStatus;

            let noti = new (obj);
            noti.save((er1, ress) => {
                if(er1){
                    console.log("Error is==========>",er1)
                }
                else{
                   Console.log("Notification saved successfully") 
                }
            })

        }
    });

}


exports.sendiosNotification = function(deviceToken1,msg,sendorId,senderName) {
console.log('fdfdrefdf',deviceToken1)
console.log('fdfretretdfdf',msg)
console.log('fdfderewfdf',sendorId)
console.log('fdfdfhfghdf',senderName)
    var options = {
        "cert": "MobiloitteEnterpriseDistribution.pem",  
        "key": "MobiloitteEnterpriseDistribution.pem",
        "passphrase": "Mobiloitte1",
        "gateway": "gateway.push.apple.com",
        "port": 2195,
        "enhanced": true,
        "cacheLength": 5
        };


    //var deviceToken = 'A30396FCDB5336BBFD6989468F0B5FEA56648E058098FD739BDA9A47EDAFF4C0';
    var deviceToken = deviceToken1;
    // console.log("iOS_notification-->>>", message)
    // console.log("senderId-->>>", senderId)
    // console.log("senderName-->>>", senderName)
        var title = "VideoPosting";
        var message = 'Text';
        var chatType = 'chatType';
        var apnConnection = new apn.Connection(options);
        // var myDevice = new apn.Device(deviceToken);
        var note = new apn.Notification();
        // console.log("mydevice-1111-->>>", myDevice)
        //note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
        note.badge = 1;
        note.alert = msg;
        note.sound = 'default';
        note.payload = { title: title, message: msg, type: "chatType" ,sendorId:sendorId, senderName:senderName};
      
        try {
        apnConnection.pushNotification(note, deviceToken)
             console.log('iOS Push Notification sendqqqqqqqqqqqqqqqqqqq');
        } catch (ex) {
         console.log("Error in push notification-- ", ex);
        }
    }




// exports.sendiosNotification =  function(deviceToken1, message, senderId, senderName) {
//   //  const apn = require('apn');
//     const fs = require('fs');
//     const options = {
//         key: fs.readFileSync(__dirname + "/mobiloitteenterprisedistribution.pem"),
//         cert: fs.readFileSync(__dirname + "/mobiloitteenterprisedistribution.pem"),
//         passphrase: "Mobiloitte1",
//         production: false
//     };
//     console.log('Test---',__dirname);
//     var apnProvider = new apn.Provider(options);
    
//     //     let apnProvider = new apn.Provider(options);
//     //     let note = new apn.Notification();
//     // var payload = {'messageFrom': 'John Appleseed'};
//     //     note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
//     //     note.badge = 0;
//     //     note.sound = "ping.aiff";
//     //     note.alert = payload['description'];
//     //    // note.topic = "com.studentteacher.studentAssessmentApp";
//     //     note.payload = payload;
//     //     note.title = payload['title'];
//     //     note.message = payload['description'];
//     //     note.type = payload['type'];
//     let deviceToken4 = "0A37B934BBD830B3F9654FBB0039D4E33B46BE4D410EF570BA9E1191D28EFD48";
//     let deviceToken = Buffer.from(deviceToken4, 'base64').toString('hex');
//     var note = new apn.Notification();
//     note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
//     note.badge = 3;
//     note.sound = "ping.aiff";
//     note.alert = "\uD83D\uDCE7 \u2709 You have a new message";
//     note.payload = {'messageFrom': 'John Appleseed'};
//     note.topic = "<your-app-bundle-id>";
//       //  var deviceTokenArray = '0A37B934BBD830B3F9654FBB0039D4E33B46BE4D410EF570BA9E1191D28EFD48'
    
    
//         apnProvider.send(note, deviceToken).then((result, err) => {
//             console.log('ios push sent--> ', JSON.stringify(result));
//             if (err) {
//                 console.log('ios push err--> ', JSON.stringify(err));
//             }
//         });
    
// }

// send ios notification function
// exports.sendiosNotification = function(deviceToken1, message, senderId, senderName, chatType) {
//     var deviceToken = '0A37B934BBD830B3F9654FBB0039D4E33B46BE4D410EF570BA9E1191D28EFD48';
//     var options = {
//                 "cert": config.iOSPemFile,
//                 "key": config.iOSPemFile,
//                 "passphrase": "Mobiloitte1",
//                 "gateway": "gateway.push.apple.com",
//                 "port": 2195,
//                 "enhanced": true,
//                 "cacheLength": 5,
//                 "title": "Brolix",
//                 "message": "Hello from Brolix. Here is a message for you!!",
//                 };
//             var title = "Brolix";
//             var message = message;
//             var chatType = chatType;
//             var apnConnection = new apn.Provider(options);
//             //var myDevice = new apn.Device(deviceToken);
//             var note = new apn.Notification();
//            // console.log("mydevice-1111-->>>", myDevice)
//             //note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
//             note.badge = 1;
//             note.alert = message;
//             note.sound = 'default';
//             note.payload = { title: title, message: message, type: chatType };
//             apnConnection.send(note, deviceToken).then((result, err) => {
//                     console.log('ios push sent--> ', JSON.stringify(result));
//             if (err) {
//                     console.log('ios push err--> ', JSON.stringify(err));
//                 }
//             });
//         // try {
//         //     apnConnection.pushNotification(note, deviceToken);
//         //     console.log('iOS Push Notification send');
//         // } catch (ex) {
//         //     console.log("Error in push notification-- ", ex);
//         // }
//     }




