const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const db = mongoose.connection;
var Schema = mongoose.Schema;
var userSchema = mongoose.Schema({
    
    email: {
        type: String,
        require: true,
        lowercase:true,
        unique:true
    },
    name: {
        type: String,
        require: true
    },
    mobileNumber: {
        type: String,
       // require: true,
      //  unique:true
      
    },
    password: {
        type: String,
        require: true
    },
    deviceToken:{
        type:String
    },
    deviceType:{
        type:String
    },
    createdAt: {
        type: Date,
        default:new Date()
    },
    createdAt1: {
        type: String,
        default: Date.now()
    },
    username:{
        type:String
    },
    profilePic: {
        type: String
     
    },
    mobileOtp:{
        type:String
    },
    emailOtp:{
        type:String
    },
    mobileOtpVerificationStatus:{
        type:Boolean,
        default:false
    },
    emailOtpVerificationStatus:{
        type:Boolean,
        default:false
    },
    usernameStatus:{
        type:Boolean,
        default:false
    },
    publicId:{
        type:String
    },
    jwtToken:{
        type:String
    },
    bio:{
        type:String
    },
    dob:{
        type:String

    },
    countryCode:{
        type:String
    },
    socialId:{
        type:String
    },
    socialType:{
        type:String
    },
    followerCount:{
        type:String,
        default:0
    },
    followers:[{
        userId:{
            type:String
        },
        isFollow:{
            type:Boolean
        }
    }],
    followingCount:{
        type:String,
        default:0
    },
    following:[{
        userId:{
            type:String
        },
        isFollow:{
            type:Boolean
        }
    }],
    // following:{
    //     type:String,
    //     default:0
    // },
    posts:{
        type:String,
        default:0
    },
    role:{
        type:String,
        default:2
    },
    status:{
        type:String,
        default:'ACTIVE'
    }
    
   
    
},{
    timestamps: true
})



userSchema.plugin(mongoosePaginate)
userSchema.plugin(mongooseAggregatePaginate);
const User = mongoose.model('user', userSchema, 'user');
module.exports = User
 //module.exports = mongoose.model('user', User, 'user');
 //const AdminModel = mongoose.model('admin', Admin, 'admin');




User.findOne({email : 'admin@admin.com'}, (error, success) => {
    if (error) {
        console.log(error)
    } else {
        if (!success) {
                    new User({
                        email: "admin@admin.com",
                        password: "$2a$10$i6z1LkIx6ZRK2nKezi65t./tI7nukmOymmkac2AHR2QI1flHYwKD6",
                        name: "Admin",
                        username:"admin",
                        countryCode:'+91',
                        mobileNumber:'9987665544',
                        profilePic: "http://eadb.org/wp-content/uploads/2015/08/profile-placeholder.jpg",
                        role:1
                    }).save((error, success) => {
                        console.log("Successfully Added Admin")
                    })
                
        }
    }
})